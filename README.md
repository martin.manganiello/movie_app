## Login
![Login](assets/login.png)
## Movies
![Movies](assets/movies.png)
## Agregar variables de entorno en el .env
* SECRET_KEY=P@ssw0rd
* DEBUG=1
* SQL_ENGINE=django.db.backends.postgresql
* SQL_DATABASE=postgres
* SQL_USER=postgres
* SQL_PASSWORD=root
* SQL_HOST=db
* SQL_PORT=5432
* DATABASE=postgres
* POSTGRES_USER=postgres
* POSTGRES_PASSWORD=root
* POSTGRES_DB=postgres
* REACT_APP_API_URL=http://127.0.0.1:8000
## Correr container
```docker-compose up -d```
## Frontend app:
localhost:3000
## Admin Django:
localhost:8000/admin
# Credentials
* user: root
* password: root